#ifndef EMPLOYEE_H
#define EMPLOYEE_H
#include <iostream>
#include <cstring>
using namespace std;

class Employee
{
	int m_Id;
	int m_Age;
	char m_strName[20];
	char m_Sex;
public:
	Employee(void){}
	Employee(int id,int age,const char* name,char sex);
	void setId(int id);
	void setAge(int age);
	void setName(const char* name);
	void setSex(char sex);
	int getId(void);
	int getAge(void);
	char* getName(void);
	char getSex(void);
	friend ostream& operator<<(ostream& os,const Employee& emp);
	friend istream& operator>>(istream& is,Employee& emp);
};

#endif

