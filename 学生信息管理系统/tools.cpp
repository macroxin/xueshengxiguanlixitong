#include "tools.h"

void clear_stdin(void)
{
	stdin->_IO_read_ptr = stdin->_IO_read_end;
}

int get_num(TYPE num)
{
	ifstream fp("id.dat",ios::in|ios::binary);
	vector<int> m;
	while(1)
	{
		int p;
		fp >> p;
		if(fp.eof()) break;
		m.push_back(p);
	}
	fp.close();
	int tmp = ++m[num-1];

	ofstream ofp("id.dat",ios::out|ios::binary);
	vector<int>::iterator it;
	for(it=m.begin();it!=m.end();it++)
	{
		ofp << *it << endl;
	}
	ofp.close();
	return tmp;
}

int get_mgrid(void)
{
	return get_num(1);
}

int get_deptid(void)
{
	return get_num(2);
}

int get_empid(void)
{
	return get_num(3);
}

int get_cmd(char start,char end)
{
	puts("--------------------");
	printf("请输入指令：");

	int cmd = 0;
	for(;;)
	{
		cmd = getch();
		if(cmd >= start && cmd <= end)
		{
			printf("%c\n",cmd);
			break;
		}
	}
	return cmd;
}

char* get_pass(char* pass,size_t len,bool flag)
{
	clear_stdin();
	size_t index = 0;

	while(index < len)
	{
		char key = getch();
		if(127 == key)
		{
			if(index > 0)
			{
				index--;
				if(flag) printf("\b \b");
			}
			continue;
		}

		if('\n' == key) break;

		pass[index++] = key;
		if(flag) printf("*");
	}

	pass[index] = '\0';
	printf("\n");
	return pass;
}

void anykey_continue(void)
{
	clear_stdin();
	puts("按任意键继续......");
	getch();
}
