#ifndef SERVICE_CTRL_IMPL_H
#define SERVICE_CTRL_IMPL_H
#include "service_ctrl.h"
#include "service_mode_impl.h"
#include "department.h"
#include "employee.h"
#include "tools.h"
using namespace std;
extern Department depm;
class ServiceCtrlImpl:public ServiceCtrl
{
	ServiceMode* srcMode;
	vector<Department> deptArr;
public:
ServiceCtrlImpl(void);
~ServiceCtrlImpl(void);
bool addDept(Department& dep);
bool delDept(int id);
vector<Department>& listDept(void);
bool addEmp(int dep_id,Employee& emp);
bool delEmp(int id);
bool modEmp(int id,Employee& emp);
Department* listEmp(int dep_id);
vector<Department>& listAllEmp(void);
};

#endif
