#include "service_ctrl_impl.h"

Department depm;

ServiceCtrlImpl::ServiceCtrlImpl(void)
{
	this->srcMode = new ServiceModeImpl;
	srcMode->load(deptArr);
	cout << "员工数据加载成功" << endl;
}
ServiceCtrlImpl::~ServiceCtrlImpl(void)
{
	srcMode->save(deptArr);
	delete srcMode;
	cout << "员工数据存储成功" << endl;
}
bool ServiceCtrlImpl::addDept(Department& dep)
{
	if(dep.getName() == NULL) return false;
	deptArr.push_back(dep);
	return true;
}
bool ServiceCtrlImpl::delDept(int id)
{
	vector<Department>::iterator it;
	for(it=deptArr.begin();it!=deptArr.end();it++)
	{
		if((*it).getId() == id)
		{
			deptArr.erase(it);
			return true;
		}
	}
	return false;
}
vector<Department>& ServiceCtrlImpl::listDept(void)
{
	return deptArr;
}
bool ServiceCtrlImpl::addEmp(int dep_id,Employee& emp)
{
	vector<Department>::iterator it;
	for(it=deptArr.begin();it!=deptArr.end();it++)
	{
		if((*it).getId() == dep_id)
		{
			(*it).empArr.push_back(emp);
			return true;
		}
	}
	return false;
}
bool ServiceCtrlImpl::delEmp(int id)
{
	vector<Department>::iterator it;
	for(it=deptArr.begin();it!=deptArr.end();it++)
	{
		vector<Employee>::iterator at;
		for(at=(*it).empArr.begin();at!=(*it).empArr.end();at++)
		{
			if((*at).getId() == id)
			{
			(*it).empArr.erase(at);
			return true;
			}
		}
	}
	return false;
}
bool ServiceCtrlImpl::modEmp(int id,Employee& emp)
{
	vector<Department>::iterator it;
	for(it=deptArr.begin();it!=deptArr.end();it++)
	{
		vector<Employee>::iterator at;
		for(at=(*it).empArr.begin();at!=(*it).empArr.end();at++)
		{
			if((*at).getId() == id)
			{
				(*it).empArr.insert((*it).empArr.erase(at),emp);
				return true;
			}
		}
	}
	return false;
}
Department* ServiceCtrlImpl::listEmp(int dep_id)
{
	vector<Department>::iterator it;
	for(it=deptArr.begin();it!=deptArr.end();it++)
	{
		if((*it).getId() == dep_id)
		{
			depm = *it;
			return &depm;
		}
	}
	return false;
	
}
vector<Department>& ServiceCtrlImpl::listAllEmp(void)
{
	return deptArr;
}

