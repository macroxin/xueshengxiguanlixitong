#include "service_view_impl.h"

ServiceViewImpl::ServiceViewImpl(void)
{
	srcCtrl = new ServiceCtrlImpl;
}
ServiceViewImpl::~ServiceViewImpl(void)
{
	delete srcCtrl;
}
void ServiceViewImpl::menuSrc(void)
{
	while(1)
	{
		system("clear");
		cout << "*******班级管理******" << endl;
		cout << "1、增加班级" << endl;
		cout << "2、删除班级" << endl;
		cout << "3、生成班级列表" << endl;
		cout << "******学生管理******" << endl;
		cout << "4、增加学生" << endl;
		cout << "5、删除学生" << endl;
		cout << "6、修改学生信息" << endl;
		cout << "7、列出班级学生" << endl;
		cout << "8、列出所有学生" << endl;
		cout << "9、退出" << endl;

		switch(get_cmd('1','9'))
		{
			case '1':addDept();break;
			case '2':delDept();break;
			case '3':listDept();break;
			case '4':addEmp();break;
			case '5':delEmp();break;
			case '6':modEmp();break;
			case '7':listEmp();break;
			case '8':listAllEmp();break;
			case '9':return;
		}
		
	}
}
void ServiceViewImpl::addDept(void)
{
	Department m;
	system("clear");
	cout << "请输入班级名称" << endl;
	char name[20];
	cin >> name;
	int id = get_deptid();
	m.setName(name);
	m.setId(id);
	if(srcCtrl->addDept(m))
	{
		cout << "班级添加成功" << endl;
		cout << "班级ID为：" << id  << endl;
	}
	else
	{
		cout << "添加失败" << endl;
	}
	anykey_continue();
}
void ServiceViewImpl::delDept(void)
{
	cout << "请输入要删除的班级ID" << endl;
	int id;
	cin >> id;
	if(id <100 || id >1000)
	{
		cout << "ID号不正确" << endl;
		return;
	}
	if(srcCtrl->delDept(id))
	{
		cout << "删除成功" << endl;
		anykey_continue();
	}
	else
	{
		cout << "删除失败" << endl;
		anykey_continue();
	}
}
void ServiceViewImpl::listDept(void)
{
	vector<Department> tmp = srcCtrl->listDept();
	vector<Department>::iterator it;
	for(it=tmp.begin();it!=tmp.end();it++)
	{
		cout << (*it).getId() << " " << (*it).getName() << " " << (*it).empArr.size() << endl;
	}
	anykey_continue();
}

void ServiceViewImpl::addEmp(void)
{
	cout << "请输入班级ID" <<endl;
	int dep_id,age,id;
	char sex;
	char name[20];
	cin >> dep_id;
	Employee m;
	cout << "请输入学生年龄" << endl;
	cin >> age;
	cout << "请输入学生姓名" << endl;
	cin >> name;
	cout << "请输入学生性别(w/m)" << endl;
	cin >>sex;
	m.setAge(age);
	m.setName(name);
	m.setSex(sex);
	id = get_empid();
	m.setId(id);
	if(srcCtrl->addEmp(dep_id,m))
	{
		cout << "添加学生成功" << endl;
		cout << "学生ID：" << id << endl;
		anykey_continue();
	}
	else
	{
		cout << "添加失败" << endl;
		anykey_continue();
	}

}
void ServiceViewImpl::delEmp(void)
{
	cout << "请输入要删除学生的ID" << endl;
	int id;
	cin >> id;
	if(id<1000)
	{
		cout << "ID不正确" << endl;
		return;
	}
	if(srcCtrl->delEmp(id))
	{
		cout << "删除成功" << endl;
		anykey_continue();
	}
	else
	{
		cout << "删除失败" << endl;
		anykey_continue();
	}
}

void ServiceViewImpl::modEmp(void)
{
	cout << "请输入学生ID" <<endl;
	int age,id;
	char sex;
	char name[20];
	cin >> id;
	Employee m;
	cout << "请输入学生年龄" << endl;
	cin >> age;
	cout << "请输入学生姓名" << endl;
	cin >> name;
	cout << "请输入学生性别(w/m)" << endl;
	cin >>sex;
	m.setAge(age);
	m.setName(name);
	m.setSex(sex);
	m.setId(id);
	if(srcCtrl->modEmp(id,m))
	{
		cout << "学生信息修改成功" << endl;
		anykey_continue();
	}
	else
	{
		cout << "修改失败" << endl;
		anykey_continue();
	}
	
}
void ServiceViewImpl::listEmp(void)
{
	cout << "请输入班级ID" << endl;
	int id;
	cin >> id;
	Department* m = srcCtrl->listEmp(id);
	cout << (*m).getId() << " " << (*m).getName() << " " << (*m).empArr.size() << endl;
	
	vector<Employee> e = (*m).empArr;
	vector<Employee>::iterator it;
	for(it=e.begin();it!=e.end();it++)
	{
			cout << (*it).getId() << " " << (*it).getName() << " " << (*it).getSex() << " " << (*it).getAge() << endl;
	}
	anykey_continue();
}
void ServiceViewImpl::listAllEmp(void)
{
	vector<Department> tmp = srcCtrl->listAllEmp();
	vector<Department>::iterator it;
	for(it=tmp.begin();it!=tmp.end();it++)
	{
		cout << (*it).getId() << " " << (*it).getName() << " " << (*it).empArr.size() << endl;
		vector<Employee>::iterator at;
		for(at=(*it).empArr.begin();at!=(*it).empArr.end();at++)
		{
			cout << (*at).getId() << " " << (*at).getName() << " " << (*at).getSex() << " " << (*at).getAge() << endl;
		}
	}
	anykey_continue();

}


