#ifndef TOOLS_H
#define TOOLS_H
#include <iostream>
#include <fstream>
#include <vector>
#include <getch.h>
#define TYPE int 
using namespace std;

void anykey_continue(void);
void clear_stdin(void);
int get_mgrid(void);
int get_deptid(void);
int get_empid(void);
int get_cmd(char start,char end);
char* get_pass(char* pass,size_t len,bool flag);
#endif
