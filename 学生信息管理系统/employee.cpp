#include "employee.h"

Employee::Employee(int id,int age,const char* name,char sex)
{
	m_Id = id;
	m_Age = age;
	strcpy(m_strName,name);
	m_Sex = sex;
}

void Employee::setId(int id)
{
	m_Id = id;
}
void Employee::setAge(int age)
{
	m_Age = age;
}
void Employee::setName(const char* name)
{
	strcpy(m_strName,name);
}
void Employee::setSex(char sex)
{
	m_Sex = sex;
}
int Employee::getId(void)
{
	return m_Id;
}
int Employee::getAge(void)
{
	return m_Age;
}
char* Employee::getName(void)
{
	return m_strName;
}
char Employee::getSex(void)
{
	return m_Sex;
}
ostream& operator<<(ostream& os,const Employee& emp)
{
	return os << emp.m_Id << " " << emp.m_Age << " " << emp.m_strName << " " << emp.m_Sex << endl;
}
istream& operator>>(istream& is,Employee& emp)
{
	return is >> emp.m_Id >> emp.m_Age >> emp.m_strName >> emp.m_Sex;
}

