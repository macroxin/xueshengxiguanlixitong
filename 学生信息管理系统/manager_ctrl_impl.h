#ifndef MANAGER_CTRL_IMPL_H
#define MANAGER_CTRL_IMPL_H
#include "manager_ctrl.h"
#include "manager_mode.h"
#include "manager_mode_impl.h"
#include "tools.h"
#include "manager.h"

using namespace std;

class ManagerCtrlImpl:public ManagerCtrl
{
	vector<Manager> mgrArr;
	ManagerMode* mgrMode;
public:
	ManagerCtrlImpl(void);
	~ManagerCtrlImpl(void);
	bool addMgr(Manager& mgr);
	bool delMgr(int id);
	vector<Manager>& listMgr(void);
};

#endif
