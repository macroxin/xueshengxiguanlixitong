#include "manager_mode_impl.h"
void ManagerModeImpl::load(vector<Manager>& mgr)
{
	fstream	tmp("managers.dat",ios::in);
	if(!tmp.good())
	{
		fstream ofs("managers.dat",ios::out|ios::binary);
		Manager m;
		m.setName("root");
		m.setPwd("123456");
		m.setId(10);
		ofs.write((char*)&m,sizeof(m));
		ofs.close();
	}
	tmp.close();

	fstream ifs("managers.dat",ios::in|ios::binary);
	if(!ifs.good())
	{
		cout << "管理员信息文件打开失败in" << endl;
		return ;
	}
	while(1)
	{
		/*int id;
		ifs >> id;
		char name[20];
		ifs >> name;
		char pwd[20];
		ifs >> pwd;
		
		Manager m(name,pwd);
		m.setId(id);
		*/
		Manager m;
		ifs.read((char*)&m,sizeof(m));
		if(ifs.eof()) break;
		mgr.push_back(m);
	}
	ifs.close();
}

void ManagerModeImpl::save(vector<Manager>& mgr)
{
	fstream ofs("managers.dat",ios::out|ios::binary);
	if(!ofs.good())
	{
		cout << "管理员信息文件打开失败out" << endl;
		return ;
	}
	
	vector<Manager>::iterator it;
	for(it=mgr.begin();it!=mgr.end();it++)
	{
		Manager m = *it;
		//ofs << m.getId() << " " << m.getName() << " " << m.getPwd() << endl;
		ofs.write((char*)&m,sizeof(m));
	}
}


