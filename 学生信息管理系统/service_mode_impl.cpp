#include "service_mode_impl.h"
void ServiceModeImpl::load(vector<Department>& dep)
{
	fstream	tmp("managers.dat",ios::in);
	if(!tmp.good())
	{
		fstream ofs("managers.dat",ios::out|ios::binary);
		ofs.close();
	}

	fstream ifs("services.dat",ios::in|ios::binary);
	if(!ifs.good())
	{
		cout << "员工信息打开失败in" << endl;
		return;
	}
	while(1)
	{
		Department d;
		ifs.read((char*)&d,sizeof(d));
		int tmp = d.empArr.size();
		if(ifs.eof()) break;
		for(int i=0;i<tmp;i++)
		{
			Employee emp;
			ifs.read((char*)&emp,sizeof(emp));
			d.empArr.push_back(emp);
		}
		dep.push_back(d);
	}
	ifs.close();
}

void ServiceModeImpl::save(vector<Department>& dep)
{
	fstream ofs("services.dat",ios::out|ios::binary);
	if(!ofs.good())
	{
		cout << "管理员信息文件打开失败out" << endl;
		return ;
	}
	vector<Department>::iterator it;
	for(it=dep.begin();it!=dep.end();it++)
	{
		Department d = *it;
		ofs.write((char*)&d,sizeof(d));
		vector<Employee>::iterator at;
		for(at=d.empArr.begin();at!=d.empArr.end();at++)
		{
			Employee e = *at;
			ofs.write((char*)&e,sizeof(e));
		}
	}
}

