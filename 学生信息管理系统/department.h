#ifndef DEPARTMENT_H
#define DEPARTMENT_H
#include <vector>
#include <iostream>
#include <fstream>
#include "employee.h"
class Department
{
	int m_Id;
	char m_strName[20];
public:
	vector<Employee> empArr;
	Department(void){}
	Department(const char* name);
	void setId(int id);
	void setName(char* name);
	int getId(void);
	char* getName(void);
	friend ostream& operator<<(ostream& os,Department& dep);
	friend istream& operator>>(istream& is,Department& dep);
};
#endif
