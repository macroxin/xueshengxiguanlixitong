#include "manager_view_impl.h"

ManagerViewImpl::ManagerViewImpl(void)
{
	mgrCtrl = new ManagerCtrlImpl;
	srcView = new ServiceViewImpl;
}

ManagerViewImpl::~ManagerViewImpl(void)
{
	delete srcView;
	delete mgrCtrl;
}
void ManagerViewImpl::loginManager(void)
{
	vector<Manager> tmp = mgrCtrl->listMgr();
	vector<Manager>::iterator it;
	system("clear");
	cout << "******企业信息管理系统******" << endl;
	cout << "请输入超级管理员用户名：" << endl;
	char name[20];
	cin >> name;
	it=tmp.begin();

	if(!strcmp((*it).getName(),name))
	{
		cout << "请输入密码:" << endl;
		char pwd[20];
		get_pass(pwd,20,true);
		if(strcmp((*it).getPwd(),pwd))
		{
			cout << "密码不正确" << endl;
			return;
		}
		menuMgr();
	}
	else
	{
		cout << "超级管理员用户名不正确" << endl;
	}
}
void ManagerViewImpl::loginService(void)
{
	vector<Manager> tmp = mgrCtrl->listMgr();
	vector<Manager>::iterator it;
	system("clear");
	cout << "******企业信息管理系统******" << endl;
	cout << "请输入管理员用户名：" << endl;
	char name[20];
	cin >> name;
	for(it=tmp.begin();it!=tmp.end();it++)
	{
		if(!strcmp((*it).getName(),name))
		{
			cout << "请输入密码:" << endl;
			char pwd[20];
			get_pass(pwd,20,true);
			if(strcmp((*it).getPwd(),pwd))
			{
				cout << "密码不正确" << endl;
				anykey_continue();
				break;
			}
			srcView->menuSrc();
		}
	}
}
void ManagerViewImpl::menuMgr(void)
{
	while(1)
	{
	system("clear");	
	cout << "******企业信息管理系统******" << endl;
	cout << "1、增加管理员" << endl;
	cout << "2、删除管理员" << endl;
	cout << "3、列出所有管理员" << endl;
	cout << "4、退出" << endl;
	
	switch(get_cmd('1','4'))
	{
		case '1':addMgr();break;
		case '2':delMgr();break;
		case '3':listMgr();break;
		case '4':return;
	}
	}
}
void ManagerViewImpl::addMgr(void)
{
	system("clear");
	Manager m;
	cout << "请输入用户名：" <<endl;
	char name[20];
	cin >> name;
	cout << "请设置密码：" << endl;
	char pwd[20];
	cin >> pwd;
	cout << "输入完成" <<endl;
	m.setName(name);
	m.setPwd(pwd);
	int id = get_mgrid();
	m.setId(id);
	if(mgrCtrl->addMgr(m))
	{
		cout << "添加管理员成功" << endl;
		cout << "管理员ID为：" << id << endl;
	}
	anykey_continue();

}
void ManagerViewImpl::delMgr(void)
{
	cout << "请输入要删除账号的ID:" << endl;
	int id;
	cin >> id;
	if(10 == id)
	{
		cout << "root账号不能删除" << endl;
		return;
	}
	if(mgrCtrl->delMgr(id))
	{
		cout << "删除成功!" << endl;
		anykey_continue();
	}
	else
	{
		cout << "删除失败!" << endl;
		anykey_continue();
	}
}
void ManagerViewImpl::listMgr(void)
{
	vector<Manager> tmp = mgrCtrl->listMgr();
	vector<Manager>::iterator it;
	for(it=tmp.begin();it!=tmp.end();it++)
	{
		cout << (*it).getId() << " " << (*it).getName() << " " << (*it).getPwd() <<endl;
	}
	anykey_continue();
}

