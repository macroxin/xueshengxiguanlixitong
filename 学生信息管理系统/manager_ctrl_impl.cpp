#include "manager_ctrl_impl.h"

ManagerCtrlImpl::ManagerCtrlImpl(void)
{
	this->mgrMode = new ManagerModeImpl;
	mgrMode->load(mgrArr);
	cout << "加载数据成功" << endl;
}

ManagerCtrlImpl::~ManagerCtrlImpl(void)
{
	mgrMode->save(mgrArr);
	delete mgrMode;
}
bool ManagerCtrlImpl::addMgr(Manager& mgr)
{
	mgrArr.push_back(mgr);
	return true;
}
bool ManagerCtrlImpl::delMgr(int id)
{
	vector<Manager>::iterator it;
	for(it=mgrArr.begin();it!=mgrArr.end();it++)
	{
		if((*it).getId() == id)
		{
			mgrArr.erase(it);
			return true;
		}
	}
	return false;
}
vector<Manager>& ManagerCtrlImpl::listMgr(void)
{
	return mgrArr;
}

