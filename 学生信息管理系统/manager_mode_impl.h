#ifndef MANAGER_MODE_IMPL_H
#define MANAGER_MODE_IMPL_H
#include <vector>
#include <iostream>
#include <fstream>
#include "manager.h"
#include "manager_mode.h"
class ManagerModeImpl:public ManagerMode
{
public:
	void load(vector<Manager>& mgr);
	void save(vector<Manager>& mgr);
};

#endif
