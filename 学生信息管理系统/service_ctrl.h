#ifndef SERVICE_CTRL_H
#define SERVICE_CTRL_H
#include "department.h"
#include "employee.h"
#include <vector>
using namespace std;
class ServiceCtrl
{
public:
	virtual ~ServiceCtrl(void) {}
	virtual bool addDept(Department& dep)=0;
	virtual bool delDept(int id)=0;
	virtual vector<Department>& listDept(void)=0;
	virtual bool addEmp(int dep_id,Employee& emp)=0;
	virtual bool delEmp(int id)=0;
	virtual bool modEmp(int id,Employee& emp)=0;
	virtual Department* listEmp(int dep_id)=0;
	virtual vector<Department>& listAllEmp(void)=0;
};

#endif
