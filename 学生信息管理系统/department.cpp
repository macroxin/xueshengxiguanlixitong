#include "department.h"

Department::Department(const char* name)
{
	strcpy(m_strName,name);
}
void Department::setId(int id)
{
	m_Id = id;
}
void Department::setName(char* name)
{
	strcpy(m_strName,name);
}
int Department::getId(void)
{
	return m_Id;
}
char* Department::getName(void)
{
	return m_strName;
}
ostream& operator<<(ostream& os,Department& dep)
{
	os << dep.m_Id << " " << dep.m_strName << " " << dep.empArr.size() << endl;
}
istream& operator>>(istream& is,Department& dep)
{
	return is >> dep.m_Id >> dep.m_strName;
}
